<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'author', 'url', 'thumb'];

    /**
     * Get the album that owns the image.
     */
    public function album()
    {
        return $this->belongsTo('App\Album');
    }

}
