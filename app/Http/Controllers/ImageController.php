<?php

namespace App\Http\Controllers;

use App\Album;
use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Import images from API
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function import(Request $request)
    {
        $url =  ("https://jsonplaceholder.typicode.com/photos");
        $json = json_decode(file_get_contents($url), true);

        foreach ($json as $jsonImage) {

            if($request->get("overwrite_old") != "1" && $request->get("add_new") != "1") {
                return redirect()->back();
            }

            $exists = Image::where('id', $jsonImage['id'])->exists();

            if((!$exists && $request->get("add_new") == "1") || ($exists && $request->get("overwrite_old") == "1")) {


                $image = Image::firstOrNew(['id'=>$jsonImage['id']]);
                $album = Album::firstOrNew(['id' => $jsonImage['albumId']]);
                $album->id = $jsonImage['albumId'];
                $album->save();
                $image->album()->associate($album);

                $image->id = $jsonImage['id'];
                $image->title = $jsonImage['title'];
                $image->url = $jsonImage['url'];
                $image->thumb = $jsonImage['thumbnailUrl'];
                $image->save();
            }
        }

        return redirect()->back();
    }

    /**
     * Edit selected image
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function edit(Request $request)
    {
        $image = Image::find($request->input("id"));
        $image->title = urldecode($request->get("title"));
        $image->author = urldecode($request->get("author"));
        $image->url = urldecode($request->get("url"));
        $image->thumb = urldecode($request->get("thumb"));
        $image->album_id = urldecode($request->get("album"));
        $image->save();

        return redirect()->back();
    }

    /**
     * Delete all images and albums
     *
     * @param
     * @return RedirectResponse
     */
    public function trunk()
    {
        Image::truncate();
        Album::truncate();
        return redirect()->back();
    }

    /**
     * Delete selected image
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function delete($id)
    {
        Image::find($id)->delete();
        return redirect()->back();
    }

}
