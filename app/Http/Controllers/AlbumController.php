<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AlbumController extends Controller
{

    /**
     * Edit selected album
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function edit(Request $request)
    {
        $image = Album::find($request->input("id"));
        $image->name = urldecode($request->get("name"));
        $image->save();
        return redirect()->back();
    }

    /**
     * Delet selected album
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function delete($id)
    {
        Album::find($id)->delete();
        return redirect()->back();
    }

    /**
     * Create album with selected name
     *
     * @param  String  $name
     * @return RedirectResponse
     */
    public function create($name)
    {
        if($name) {
            $album = new Album();
            $album->name = urldecode($name);
            $album->save();
        }

        return redirect()->back();
    }
}
