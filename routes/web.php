<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});



Route::get('import', 'ImageController@import');


Route::get('album/create/{name}', 'AlbumController@create');
Route::get('album/edit/', 'AlbumController@edit');
Route::get('album/delete/{id}', 'AlbumController@delete');
Route::get('/album/view/{id}', function ($id) {
    //return view('images', $id);
    return View::make('images')->with('id', $id);

});

Route::get('image/edit/', 'ImageController@edit');
Route::get('image/delete/{id}', 'ImageController@delete');

Route::get('/trunk', 'ImageController@trunk');
