<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div class="album py-5 bg-light">
    <div class="container">
        <form method="GET" action="/import">
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" name="overwrite_old" value="1" type="checkbox" id="overwrite_old">
                    <label class="form-check-label" for="gridCheck">
                        Overwrite images in db
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" name="add_new" value="1" type="checkbox" id="add_new">
                    <label class="form-check-label" for="gridCheck">
                        Add new from JSON
                    </label>
                </div>
            </div>

            <button type="submit" id="importButton" class="btn btn-secondary">Import</button>
            <button onclick="createAlbum()" type="button" id="createAlbumButton" class="btn btn-secondary">Create album</button>
        </form>

        <div class="row">
        @foreach(\App\Album::all() as $album)
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <img class="img-thumbnail img-fluid" src="@if($album->images()->count()>0) {{$album->images()->first()->url}} @endif">
                    <div class="card-body">
                        <p class="card-text">{{$album->name}}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button onclick="window.location.href = '/album/view/{{$album->id}}'" type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#editModal" data-name="{{$album->name}}" data-id="{{$album->id}}">Edit</button>
                                <button onclick="window.location.href = '/album/delete/{{$album->id}}'" type="button" class="btn btn-sm btn-outline-secondary">Delete</button>

                            </div>
                            <small class="text-muted">id {{$album->id}}</small>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <!-- Button trigger modal -->


        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Edit album</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Name of album:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="saveChanges()">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var editingId=0;
            $('#editModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget)
                var recipient = button.data('id')
                editingId = recipient
                var name = button.data('name')
                var modal = $(this)
                modal.find('.modal-title').text('Edit album ID ' + recipient)
                modal.find('.modal-body input').val(name)
            })

            function saveChanges() {
                window.location.href = decodeURIComponent("album/edit/?id=" + editingId + "&" + "name=" + $('#editModal').find('.modal-body input').val());
            }
            function createAlbum() {
                var albumName = prompt('Album name?');
                window.location.href = "album/create/" + albumName;

            }

        </script>
<button type="button" class="btn btn-secondary" onclick="window.location.replace('/trunk')">Delete all images and albums</button>

</html>
