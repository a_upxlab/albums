<html>
<meta charset="UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<div class="album py-5 bg-light">
    <div class="container">
        <button type="button" class="btn btn-secondary" onclick="window.location.replace('/')">To albums</button>
        <div class="row">
        @foreach(\App\Image::where('album_id', $id)->get() as $image)
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <img class="img-thumbnail img-fluid" src="{{$image->url}}">
                    <div class="card-body">
                        <p class="card-text">{{$image->title}}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button onclick="window.location.href = '/album/view/{{$image->id}}'" type="button" class="btn btn-sm btn-outline-secondary">View</button>
                                <button type="button" class="btn btn-sm btn-outline-secondary" data-toggle="modal" data-target="#editModal" data-title="{{$image->title}}" data-id="{{$image->id}}" data-author="{{$image->description}}" data-url="{{$image->url}}" data-thumb="{{$image->thumb}}" >Edit</button>
                                <button onclick="window.location.href = '/image/delete/{{$image->id}}'" type="button" class="btn btn-sm btn-outline-secondary">Delete</button>

                            </div>
                            <small class="text-muted">id {{$image->id}}</small>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Edit album</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Title:</label>
                                <input type="text" class="form-control" id="title">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Author:</label>
                                <input type="text" class="form-control" id="author">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">URL:</label>
                                <input type="text" class="form-control" id="url">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Thumbnail:</label>
                                <input type="text" class="form-control" id="thumb">
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Album:</label>
                            <select class="custom-select" id="album">
                                <option selected>Choose...</option>
                                @foreach(\App\Album::all() as $album)
                                    <option value="{{$album->id}}">id {{$album->id}}</option>
                                @endforeach
                            </select>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="saveChanges()">Save</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            var editingId=0;
            $('#editModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('id') // Extract info from data-* attributes
                editingId = recipient;
                var title = button.data('title')
                var author = button.data('author')
                var url = button.data('url')
                var thumb = button.data('thumb')
                var album = {{$id}}
                var modal = $(this);
                modal.find('.modal-title').text('Edit image ID ' + recipient);
                $("#title").val(title);
                $("#author").val(author);
                $("#url").val(url);
                $("#thumb").val(thumb);
                $("#album").val(album);
            })

            function urldecode(url) {
                return decodeURIComponent(url.replace(/\+/g, ' '));
            }

            function saveChanges() {
                var params = {id:editingId, title:$("#title").val(), author:$("#author").val(), url:$("#url").val(), thumb:$("#thumb").val(), album:$("#album").val()};
                window.location.replace("/image/edit/?" + jQuery.param(params));

            }

        </script>
</html>
